// title      : showerSpacer
// author     : John Cole
// license    : ISC License
// file       : showerSpacer.jscad

/* exported main, getParameterDefinitions */
/* tslint:disable:2304 */

function getParameterDefinitions() {
  return [
    {
      name: 'resolution',
      type: 'choice',
      values: [0, 1, 2, 3],
      captions: [
        'normal (12,32)',
        'high (24,64)',
        'very high (48,128)',
        'fine (96,192)'
      ],
      default: 1,
      initial: 1,
      caption: 'Resolution:'
    },
    {
      type: 'group',
      name: 'spacerGroup',
      caption: 'Spacer'
    },
    {
      type: 'float',
      name: 'wireDelta',
      caption: 'Wire Clearance (mm):',
      initial: 20
    },
    {
      type: 'group',
      name: 'pipeGroup',
      caption: 'Pipe'
    },
    {
      type: 'float',
      name: 'pipeDiameter',
      caption: 'Diameter (mm):',
      initial: 20.8 + 0.5
    },
    {
      type: 'group',
      name: 'wireGroup',
      caption: 'Wire'
    },
    {
      type: 'float',
      name: 'outerDiameter',
      caption: 'Outer Diameter (mm):',
      initial: 21.275 * 2
    },
    {
      type: 'float',
      name: 'innerDiameter',
      caption: 'Inner Diameter (mm):',
      initial: 32
    },
    {
      type: 'group',
      name: 'parts',
      caption: 'Parts'
    },
    {
      type: 'checkbox',
      name: 'spacer',
      checked: true,
      caption: 'Spacer:'
    },
    {
      type: 'checkbox',
      name: 'pipe',
      checked: false,
      caption: 'Pipe:'
    },
    {
      type: 'checkbox',
      name: 'wire',
      checked: false,
      caption: 'Wire:'
    }
  ];
}

function main(params) {
  var resolutions = [[12, 32], [24, 64], [48, 128], [96, 192]];
  var [defaultResolution3D, defaultResolution2D] = resolutions[
    parseInt(params.resolution)
  ];
  CSG.defaultResolution3D = defaultResolution3D;
  CSG.defaultResolution2D = defaultResolution2D;
  util.init(CSG);
  var options = {
    outerRadius: params.outerDiameter / 2,
    innerRadius: params.innerDiameter / 2,
    diameter: 5.15 + 0.5,
    length: params.wireDelta,
    innerResolution: defaultResolution3D,
    outerResolution: defaultResolution3D
  };
  var pipeOptions = {
    diameter: params.pipeDiameter
  };
  // console.log({ params, options, pipeOptions });
  var wireDelta = params.wireDelta;
  var spacer = Spacer(options);
  var wire = Wire(options).align(spacer, 'z');
  var pipe = cylinder({
    r: pipeOptions.diameter / 2,
    h: 20,
    fn: defaultResolution3D
  })
    .snap(spacer, 'x', 'inside+')
    .translate([-(options.diameter + wireDelta), 0, 0])
    .align(spacer, 'z')
    .color('silver');

  var pipeCutout = pipe
    .stretch('x', options.length * 2)
    .snap(spacer, 'x', 'inside+')
    .translate([-(options.diameter + wireDelta), 0, 0])
    .color('orange');

  var parts = [];
  if (params.spacer) parts.push(spacer.subtract(pipeCutout).subtract(wire));
  if (params.pipe) parts.push(pipe);
  if (params.wire) parts.push(wire);
  return parts;
}

function Spacer(options) {
  var od =
    (options.innerRadius + (options.outerRadius - options.innerRadius) / 2) * 2;
  var top = Parts.Cylinder(od, options.diameter + 5, {
    resolution: options.outerResolution
  }).bisect('x').parts.positive;

  var bottom = Parts.Cube([options.length, od, options.diameter + 5]);
  return top
    .union(bottom.align(top, 'y').snap(top, 'x', 'outside+', 0.00000000000001))
    .color('blue');
}

function Wire(options = {}) {
  options = Object.assign(
    {
      outerRadius: 30,
      innerRadius: 25,
      innerResolution: CSG.defaultResolution3D,
      outerResolution: CSG.defaultResolution3D
    },
    options
  );
  var wire = torus({
    ri: options.diameter / 2,
    fni: options.innerResolution,
    fno: options.outerResolution,
    ro: options.innerRadius + (options.outerRadius - options.innerRadius) / 2
  }).bisect('x').parts.positive;
  var wireend = cylinder({
    r: options.diameter / 2,
    h: options.length,
    fn: options.outerResolution
  })
    .rotateY(90)
    .rotateX(90)
    .snap(wire, 'x', 'outside+', 0.00000000000001);
  return union([
    wire,
    wireend.snap(wire, 'y', 'inside-'),
    wireend.snap(wire, 'y', 'inside+')
  ]).color('silver');
}

// ********************************************************
// Other jscad libraries are injected here.  Do not remove.
// Install jscad libraries using NPM
// ********************************************************
// include:js
// endinject
